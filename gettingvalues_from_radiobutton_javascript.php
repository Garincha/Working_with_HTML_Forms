<?php

?>
<html>
    <head>
        <title>PHP Array Functions</title>
        <style>
            .code{width:900px; margin: 0 auto; background:<?php echo '#ddd';?>;text-align: center;}
            .headeroption,.footeroptions{background: #444;color: #fff;text-align: center;padding: 20px;}
            .maincontent{min-height: 300px;padding: 20px;}
            .headeroption h2,.footeroptions h2{margin: 0;}
        </style>
        
    </head>
    
    <body>
        <div class="code">
            <div class="headeroption">
                <h2><?php echo 'PHP Fundamentals'?></h2>
            </div>
            <div class="maincontent">
                <script>
                    function clickHere(){
                        var legth = document.myform.gender.length;
                        for(i = 0;i < legth;i++){
                            var checkValue = document.myform.gender[i].checked;
                            if(checkValue){
                               var checkResult = document.myform.gender.value;
                               
                            }
                        }
                        var showData = "Your Gender is : "+checkResult
                        document.getElementById('show').innerHTML = showData;
                    }
                </script>
                <div id="show"></div>
                <form action="" method="post" id="myform" name="myform" onsubmit="clickHere(); return false;">
                    <table>
                        <tr>
                            <td>
                                Gender :
                            </td>
                            <td>
                                <input type="radio" name="gender" value="Male">Male
                                <input type="radio" name="gender" value="Female">Female
                                <input type="radio" name="gender" value="Others">Others
                            </td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>
                                <input type="submit" value="Submit">
                                <input type="reset" value="Reset">
                            </td>
                        </tr>
                    </table>
                </form>
            </div>
                  
            <div class="footeroptions">
                <h2><?php echo 'www.w3schools.com';?></h2>
            </div>
            
        </div>
       
    </body>
</html>


