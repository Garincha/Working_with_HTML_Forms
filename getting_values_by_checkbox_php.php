<?php

?>
<html>
    <head>
        <title>PHP Array Functions</title>
        <style>
            .code{width:900px; margin: 0 auto; background:<?php echo '#ddd';?>;text-align: center;}
            .headeroption,.footeroptions{background: #444;color: #fff;text-align: center;padding: 20px;}
            .maincontent{min-height: 300px;padding: 20px;}
            .headeroption h2,.footeroptions h2{margin: 0;}
        </style>
        
    </head>
    
    <body>
        <div class="code">
            <div class="headeroption">
                <h2><?php echo 'PHP Fundamentals'?></h2>
            </div>
            <div class="maincontent">
                <?php
                   if(isset($_POST['click']) && !empty($_POST['click'])){
                       $cod = $_POST['coder'];//the value of coder comes through the key click
                       echo 'You have selected ';
                       foreach ($cod as $show){
                           echo $show.", ";
                       }
                   } 
                ?>
                <form action="" method="post" id="myform" name="myform">
                    <table>
                        <tr>
                            <td>
                                Coder :
                            </td>
                            <td>
                                <input type="checkbox" name="coder[]" value="php">PHP<!--here we are using []-<(this sign indicates an array) after the key coder, to give user an option to select multiple check box at a time. -->
                                <input type="checkbox" name="coder[]" value="javascript">Javascript
                                <input type="checkbox" name="coder[]" value="python">Python
                            </td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>
                                <input type="submit" name = "click" value="Submit">
                                <input type="reset" value="Reset">
                            </td>
                        </tr>
                    </table>
                </form>
            </div>
                  
            <div class="footeroptions">
                <h2><?php echo 'www.w3schools.com';?></h2>
            </div>
            
        </div>
       
    </body>
</html>



