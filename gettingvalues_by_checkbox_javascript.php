<?php

?>
<html>
    <head>
        <title>PHP Array Functions</title>
        <style>
            .code{width:900px; margin: 0 auto; background:<?php echo '#ddd';?>;text-align: center;}
            .headeroption,.footeroptions{background: #444;color: #fff;text-align: center;padding: 20px;}
            .maincontent{min-height: 300px;padding: 20px;}
            .headeroption h2,.footeroptions h2{margin: 0;}
        </style>
        
    </head>
    
    <body>
        <div class="code">
            <div class="headeroption">
                <h2><?php echo 'PHP Fundamentals'?></h2>
            </div>
            <div class="maincontent">
                <script>
                    function clickCheck(){
                        var length = document.myform.coder.length;
                        var $result = "";
                        for(i = 0;i < length;i++){
                            var checkValue = document.myform.coder[i].checked;
                            if(checkValue){
                               $result += document.myform.coder[i].value+", ";
                               
                            }
                        }
                        var showData = "Your Passion is : "+$result
                        document.getElementById('show').innerHTML = showData;
                    }
                   
                </script>
                <div id="show"></div>
                <form action="" method="post" id="myform" name="myform" onsubmit="clickCheck();return false;">
                    <table>
                        <tr>
                            <td>
                                Coder :
                            </td>
                            <td>
                                <input type="checkbox" name="coder" value="php">PHP
                                <input type="checkbox" name="coder" value="javascript">Javascript
                                <input type="checkbox" name="coder" value="python">Python
                            </td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>
                                <input type="submit" name = "click" value="Submit">
                                <input type="reset" value="Reset">
                            </td>
                        </tr>
                    </table>
                </form>
            </div>
                  
            <div class="footeroptions">
                <h2><?php echo 'www.w3schools.com';?></h2>
            </div>
            
        </div>
       
    </body>
</html>




