<?php

?>
<html>
    <head>
        <title>PHP Array Functions</title>
        <style>
            .code{width:900px; margin: 0 auto; background:<?php echo '#ddd';?>;text-align: center;}
            .headeroption,.footeroptions{background: #444;color: #fff;text-align: center;padding: 20px;}
            .maincontent{min-height: 300px;padding: 20px;}
            .headeroption h2,.footeroptions h2{margin: 0;}
        </style>
        
    </head>
    
    <body>
        <div class="code">
            <div class="headeroption">
                <h2><?php echo 'PHP Fundamentals'?></h2>
            </div>
            <div class="maincontent">
                <script>
                    function selectOption(){
                        var index = document.myform.coder.selectedIndex;
                        var value = document.myform.coder.options[index].value; 
                        var showdata = "You are "+value+ " coder";
                        document.getElementById('show').innerHTML = showdata;
                    }
                </script>
                <div id="show"></div>
                <form action="" method="post" id="myform" name="myform" onsubmit="selectOption(); return false;">
                    <table>
                        <tr>
                            <td>
                                Language :
                            </td>
                            <td>
                                <select name="coder">
                                    <option>Select One</option>
                                    <option value="php">PHP</option>
                                    <option value="js">JavaScript</option>
                                    <option value="c">C</option>
                                    <option value="perl">Perl</option>
                                    <option value="ruby">Ruby</option>
                                    
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>
                                <input type="submit" name = "click" value="Submit">
                                <input type="reset" value="Reset">
                            </td>
                        </tr>
                    </table>
                </form>
            </div>
                  
            <div class="footeroptions">
                <h2><?php echo 'www.w3schools.com';?></h2>
            </div>
            
        </div>
       
    </body>
</html>




