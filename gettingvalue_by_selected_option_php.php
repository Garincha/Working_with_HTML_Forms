<?php

?>
<html>
    <head>
        <title>PHP Array Functions</title>
        <style>
            .code{width:900px; margin: 0 auto; background:<?php echo '#ddd';?>;text-align: center;}
            .headeroption,.footeroptions{background: #444;color: #fff;text-align: center;padding: 20px;}
            .maincontent{min-height: 300px;padding: 20px;}
            .headeroption h2,.footeroptions h2{margin: 0;}
        </style>
        
    </head>
    
    <body>
        <div class="code">
            <div class="headeroption">
                <h2><?php echo 'PHP Fundamentals'?></h2>
            </div>
            <div class="maincontent">
                <?php
                   if(isset($_POST['click']) && !empty($_POST['click'])){
                       $cod = $_POST['coder'];//the value of coder comes through the key click.
                       if($cod == "php"){
                           echo "You have selected PHP";
                       }elseif ($cod == "js") {
                           echo 'You have selected Javascript';
                        }elseif ($cod == "c") {
                            echo 'You have selected C';
                        }elseif ($cod == "perl") {
                            echo 'You have selected Perl';
                        }elseif ($cod == "ruby") {
                            echo 'You have selected Ruby';
                        }  else {
                            echo 'You have selected nothing';
                        }
                   } 
                ?>
                <form action="" method="post" id="myform" name="myform">
                    <table>
                        <tr>
                            <td>
                                Language :
                            </td>
                            <td>
                                <select name="coder">
                                    <option>Select One</option>
                                    <option value="php">PHP</option>
                                    <option value="js">JavaScript</option>
                                    <option value="c">C</option>
                                    <option value="perl">Perl</option>
                                    <option value="ruby">Ruby</option>
                                    
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>
                                <input type="submit" name = "click" value="Submit">
                                <input type="reset" value="Reset">
                            </td>
                        </tr>
                    </table>
                </form>
            </div>
                  
            <div class="footeroptions">
                <h2><?php echo 'www.w3schools.com';?></h2>
            </div>
            
        </div>
       
    </body>
</html>




