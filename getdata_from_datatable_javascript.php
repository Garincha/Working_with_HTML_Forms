<?php

?>
<html>
    <head>
        <title>PHP Array Functions</title>
        <style>
            .code{width:900px; margin: 0 auto; background:<?php echo '#ddd';?>;text-align: center;}
            .headeroption,.footeroptions{background: #444;color: #fff;text-align: center;padding: 20px;}
            .maincontent{min-height: 300px;padding: 20px;}
            .headeroption h2,.footeroptions h2{margin: 0;}
            #myform{width:400px;border:1px solid #fff;padding:10px;}
            .tblone{width:400px;border:1px solid #fff;margin:20px 0}
            .tblone td{padding:5px 10px;}
            table.tblone tr:nth-child(2n+1){background:#fff;height:30px;}
            table.tblone tr:nth-child(2n){background:#f1f1f1;height:30px;}
        </style>
        
    </head>
    
    <body>
        <div class="code">
            <div class="headeroption">
                <h2><?php echo 'PHP Fundamentals'?></h2>
            </div>
            <div class="maincontent">
                <script>
                    function clickOption(){
                      var getname = document.myform.your_name.value; 
                      document.getElementById('showName').innerHTML = getname;
                      
                      var genderlength = document.myform.gender.length;
                      for(i = 0;i < genderlength ;i++){
                      var gendercheck = document.myform.gender[i].checked;
                        if(gendercheck){
                            genderValue = document.myform.gender[i].value;
                        }
                        }
                        document.getElementById('showGender').innerHTML = genderValue;
                        
                      var chkboxlength = document.myform.dep.length;
                      for(i = 0;i < chkboxlength ;i++){
                      var chkboxcheck = document.myform.dep[i].checked;
                        if(chkboxcheck){
                            chkboxValue = document.myform.dep[i].value;
                        }
                        }
                        document.getElementById('showChkbox').innerHTML = chkboxValue; 
                        
                      var index = document.myform.cod.selectedIndex;
                      var coder = document.myform.cod.options[index].value;
                       document.getElementById('showOptions').innerHTML = coder; 
                    }
                </script>
                <table class="tblone">
                    <tr>
                        <td colspan="2" align="center">Output</td>
                    </tr>
                    <tr>
                        <td>Name</td>
                        <td><span id="showName"></span></td>
                    </tr>
                    <tr>
                        <td>Gender</td>
                        <td><span id="showGender"></span></td>
                    </tr>
                    <tr>
                        <td>Department</td>
                        <td><span id="showChkbox"></span></td>
                    </tr>
                    <tr>
                        <td>Coder</td>
                        <td><span id="showOptions"></span></td>
                    </tr>
                </table>
                <form action="" method="post" id="myform" name="myform" onsubmit="clickOption(); return false;">
                    <table>
                        <tr>
                            <td>Name : </td>
                            <td><input type="text" name="your_name" required="1"></td>
                        </tr>
                        <tr>
                            <td>Gender : </td>
                            <td>
                                <input type="radio" name="gender" value="Male">Male
                                <input type="radio" name="gender" value="Female">Female
                            </td>
                        </tr>
                        <tr>
                            <td>Department : </td>
                            <td>
                                <input type="checkbox" name="dep" value="CSE">CSE
                                <input type="checkbox" name="dep" value="EEE">EEE
                                <input type="checkbox" name="dep" value="Physics">Physics
                                <input type="checkbox" name="dep" value="Chemistry">Chemistry
                            </td>
                        </tr>
                        <tr>
                            <td>Coder : </td>
                            <td>
                                <select name="cod" required="1">
                                    <option value="">Select One</option>
                                    <option value="PHP">PHP</option>
                                    <option value="Java">Java</option>
                                    <option value="Perl">Perl</option>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>
                                <input type="submit" name = "click" value="Submit">
                                <input type="reset" value="Reset">
                            </td>
                        </tr>
                    </table>
                </form>
            </div>
                  
            <div class="footeroptions">
                <h2><?php echo 'www.w3schools.com';?></h2>
            </div>
            
        </div>
       
    </body>
</html>






